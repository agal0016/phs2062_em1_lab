"""
Currently just the double-slit part.
"""
#%%
import matplotlib.pyplot as plt
import numpy as np
import sympy as sp
from scipy import stats
import monashspa.PHS2061 as spa
# from monashspa.PHS2062 import PHS2062_gas_fit

from sympy_help import make_symbols, uncertainty

CM = 0.01
DEG = np.pi / 180
MILLI = 10 ** (-3)


#%%
################################ READING DATA ################################
data_polar = np.genfromtxt("data/EM1_twoslit.csv", delimiter=",", skip_header=1)
# Ok, sort data by n (first column) from -2 increasing to 2
data_polar = data_polar[data_polar[:,0].argsort()]
# Now transpose so each row now corresponds to a different variable
data_polar = data_polar.transpose()

# Parsing tabular data into single arrays
min_max, angle, u_angle, Vp2p, u_Vp2p = data_polar
angle *= DEG
u_angle *= DEG
Vp2p *= MILLI
u_Vp2p *= MILLI

print(f"min_max={min_max}")
print(f"angle={angle}")
print(f"u_angle={u_angle}")
print(f"Vp2p={Vp2p}")
print(f"u_Vp2p={u_Vp2p}")

# Other data not included in csv file
aperture = 1.5 * CM
u_aperture = 0.2 * CM * 2 # Gets doubled due to wonky slit setup!
slit_separation = 5 * CM
u_slit_separation = 0.2 * CM

#%%
# syms = make_symbols("d t n")
# d, t, n = syms
# u_syms = make_symbols("u(d) u(t) u(n)")

# Okay, so normally maxima are given by
#       d sin (t) = n l
# and minima are given by
#       d sin (t) = (n + 1/2) l
# HOWEVER, because n is defined in the data such that
#       n = 0 is central max
#       n = 1 is 1st minimum to right
#       n = 2 is 1st maximum to right
#       n = -1 is 1st minimum to left
#       n = -2 is 1st maximum to left
# we can actually use a single formula to just figure out where EITHER
# constructive or destructive interference occurs:
#       d sin (t) = n/2 l
# Therefore,
#       l = 2/n d sin (t)


# l = 2 / n * d * sp.sin(t)
# u_l = uncertainty(l, syms, u_syms)

# l_func = sp.lambdify([*syms], l)
# u_l_func = sp.lambdify([*syms, *u_syms], u_l)

#%%
# Use midpoints of slits rather than just the separation between their
# edges!
true_separation = slit_separation + aperture
u_true_separation = u_slit_separation + u_aperture

# l_vals = l_func(true_separation, angle, min_max)
# u_l_vals = u_l_func(true_separation, angle, min_max, u_true_separation, u_angle, 0)

# %%
# Just defining function to give me uncertainty in our "y" values
def y_and_uy(true_separation, u_true_separation, angle, u_angle):
    syms = make_symbols("d t")
    d, t = syms
    u_syms = make_symbols("u(d) u(t)")

    y = d * sp.sin(t)
    u_y = uncertainty(y, syms, u_syms)

    y_func = sp.lambdify([*syms], y)
    u_y_func = sp.lambdify([*syms, *u_syms], u_y)

    y_vals = y_func(true_separation, angle)
    u_y_vals = u_y_func(true_separation, angle, u_true_separation, u_angle)
    return y_vals, u_y_vals


# %%
# Applying fit to linear relationship now. Gradient will give us lambda!
# Based on: 
#       d sin (t) = n/2 l
# y = d sin(t)
# x = n/2
# gradient = l
# intercept should equal zero
y, u_y = y_and_uy(true_separation, u_true_separation, angle, u_angle)
x = min_max / 2
model = spa.linear_fit(x=x, y=y, u_y=u_y)
fit_params = spa.get_fit_parameters(model)
u_fit = model.eval_uncertainty(sigma=1)

# Interpreting fit gradient
wavelength = fit_params["slope"]
u_wavelength = fit_params["u_slope"]
print(f"Wavelength: {wavelength:.3f} ± {u_wavelength:.3f} m (1 uncertainty).")


# %%
# Plotting d sin(t) against n/2
plt.errorbar(
    x,
    y,
    yerr=u_y,
    linestyle="none",
    marker="x",
    color="red",
    markersize=8,
    label="Experimental",
)
plt.plot(x, model.best_fit, "--", label="Fit")
fit_uncertainty = 1 * u_fit
plt.fill_between(x, model.best_fit - fit_uncertainty, model.best_fit + fit_uncertainty, alpha=0.4, color="lightgrey", label="Uncertainty in fit (3 sigma)")
plt.xlabel(r"$n/2$")
plt.ylabel(r"$d \sin(\theta)$")
plt.title(r"$d \sin(\theta) = \frac{n}{2} \lambda$")
plt.grid()
plt.legend()
plt.show()
# %%
######################## ATTEMPTING TO PLOT PULL PLOT ########################
pull = (y - model.best_fit) / u_fit
plt.figure("pull_plot")
plt.hlines(y=[-1, 1], xmin=-1, xmax=1, linestyles="dashed", colors="grey")
plt.hlines(y=0, xmin=-1, xmax=1, linestyles="solid", colors="grey")
plt.plot(x, pull, label="Experimental data", linestyle="none", marker="o", color="red", markersize=8)
plt.ylabel("Pull")
plt.xlabel("$n / 2$")
plt.title("Pull plot for linear fit")
plt.show()
# %%
################## PLOTTING RAW DATA (ANGLE VERSUS VOLTAGE) ##################
plt.figure("raw_data")
plt.errorbar(angle/DEG, Vp2p, xerr=u_angle/DEG, yerr=u_Vp2p, label="Experimental data", linestyle="none", marker="x", color="red", markersize=8)
plt.xlabel("Angle [º]")
plt.ylabel("Peak-to-peak voltage at receiver [V]")
plt.title("Angle against peak-to-peak voltage")
plt.grid()
plt.show()
# %%
#### Printing p value
pvalue = 1.0 - stats.chi2.cdf(model.chisqr, model.ndata-model.nvarys)

print("""
=================
  p-value       = {pvalue:.2E}
""".format(pvalue=pvalue))
print(model.fit_report())
# %%
