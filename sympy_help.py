"""
sympy_help.py
–––––––––––––

Author: Lex Gallon, Monash University.
Student no.: 30579821

This module simply contains two helpful functions:
* `make_symbols()`: For creating a numpy array of sympy Symbol objects.
* `uncertainty()`: For easily calculating the uncertainty in any expression
  given arrays of its symbolic variables and uncertainties.


"""
import numpy as np
import sympy as sp

def make_symbols(names):
    """
    Returns a numpy array of sympy symbols. `syms` gets passed into
    `sympy.symbols()`, so make sure it's an array of strings OR a single string
    with space separating each symbol.

    Parameters
    ----------
    names: str || str[]
        Can be comma-or-whitespace delimited string of variable names, or an
        array of string variable names. Is passed into `sympy.symbols()`.

    Returns
    -------
    symbols: numpy.ndarray containing sympy Symbols
    """
    return np.array(sp.symbols(names))


def uncertainty(expr, syms, u_syms):
    """
    Returns the total uncertainty in a multi-variable expression. Make sure
    that syms and u_syms are in the exact same order. For example, if
    syms == [T, m, n], then u_syms should be something like [u_t, u_m, u_n].

    Parameters
    ----------
    expr: sympy.Expr
        The expression for the derived quantity of interest.
    syms: array || numpy.ndarray
        Array of symbols found in the expression. I.e., these would be all the
        variables you take partial derivatives with respect to.
    u_syms: array || numpy.ndarray
        Like `syms`, except these represent the uncertainties for each
        variable. NOTE: These need to be in the same order as their
        corresponding `syms` in order to ensure each partial derivative is
        correctly multiplied by its associated uncertainty!

    Returns
    -------
    u_expr: sympy.Expr
        An expression representing the uncertainty in `expr`, in terms of all
        variables and their uncertainties.
    """
    if not isinstance(syms, np.ndarray):
        syms = np.array(syms)
    if not isinstance(u_syms, np.ndarray):
        u_syms = np.array(u_syms)

    # Differentiate with respect to each variable once
    derivs = [expr.diff(sym) for sym in syms]
    derivs = np.array(derivs)
    # Multiply each derivative by its corresponding uncertainty
    inside = derivs * u_syms  # element-wise multiplication
    # Add in quadrature
    inside = np.dot(inside, inside)
    result = sp.sqrt(inside)
    return result